const 国語 = {
    太郎:59,
    瑞希:100,
    ニチロー:33,
};

const 数学 = {
    太郎:89,
    瑞希:100,
    ニチロー:12,
};

const 英語 = {
    太郎:49,
    瑞希:100,
    ニチロー:97,
};

const subject = (person) => {
  return "太郎:" + person.太郎 + "  " + "瑞希:" + person.瑞希 + "  " + "ニチロー:" + person.ニチロー;
}

console.log(subject(国語));
console.log(subject(数学));
console.log(subject(英語));

